provider "aws" {
  region = "eu-central-1"
}

resource "aws_vpc" "actpro-vpc" {
     cidr_block = "10.0.0.0/16"
      tags = {
        Name = "ActPRO-NET"
  }
}

resource "aws_subnet" "front-end-net" {
  vpc_id     = aws_vpc.actpro-vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "front-net"
  }
}

resource "aws_subnet" "back-end-net" {
  vpc_id     = aws_vpc.actpro-vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "back-net"
  }
}

resource "aws_internet_gateway" "actpro-gw" {
  vpc_id = aws_vpc.actpro-vpc.id

  tags = {
    Name = "actpro-gw"
  }
}

resource "aws_route_table" "actpro-rt" {
  vpc_id = aws_vpc.actpro-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.actpro-gw.id
  } 

  tags = {
    Name = "actpro-rt-front"
  }
}

resource "aws_route_table_association" "a-front-net" {
  subnet_id      = aws_subnet.front-end-net.id
  route_table_id = aws_route_table.actpro-rt.id
}

resource "aws_security_group" "actpro-sg" {
  name        = "ssh-web"
  description = "Allow 22 and 80 ports traffic"
  vpc_id      = aws_vpc.actpro-vpc.id
  
## rules
  dynamic "ingress" {
    for_each = ["22", "80", "443", "8080", "5432", "3000"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  tags = {
    Name = "ssh-web-sg"
  }
}

data "aws_ami" "ubuntu-latest" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical 
}

## create app-server
resource "aws_instance" "app-server" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net.id
  vpc_security_group_ids = [aws_security_group.actpro-sg.id]
  associate_public_ip_address = true
  key_name = "po1ison_ssh"
  tags = {
    Name = "app_server"
  }
}

## create db-server
resource "aws_instance" "db-server" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net.id
  vpc_security_group_ids = [aws_security_group.actpro-sg.id]
  associate_public_ip_address = true
  key_name = "po1ison_ssh"
  tags = {
    Name = "db-server"
  }
}


## create db-replica server
resource "aws_instance" "db-replica" {
  ami           = data.aws_ami.ubuntu-latest.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.front-end-net.id
  vpc_security_group_ids = [aws_security_group.actpro-sg.id]
  associate_public_ip_address = true
  key_name = "po1ison_ssh"
  tags = {
    Name = "db-replica"
  }
}


## create file "hosts"
resource "local_file" "inventory" {
  filename = "/home/ubuntu/ansible/hosts"
  content = <<-EOT
    # APP service
    [app]
    ${join("\n", aws_instance.app-server[*].public_ip)}

    # DB service
    [db]
    ${join("\n", aws_instance.db-server[*].public_ip)}

    # DB replica
    [db_replica]
    ${join("\n", aws_instance.db-replica[*].public_ip)}
    EOT
}

