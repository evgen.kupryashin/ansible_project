This project is deploying three servers to Amazon AWS:
app_server
db_server
db_replica server.
Also with the help of ansible we deploy Todo, postgresql applications, set up database replication.

1. Run **terraform apply** file **main.tf** to create VM
2. Run **ansible-playbook main.yml** to deploy software
